package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projet2testApplication {

	public static void main(String[] args) {
		SpringApplication.run(Projet2testApplication.class, args);
	}

}
