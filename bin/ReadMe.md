# Gestion PSW

##Projet de stage

###Introduction

Ce document portera sur :

-La liste des fonctionnalités indiquées dans le cahier des charges.

-La façon dont le logiciel a été développé (sa description technique ainsi que les outils de développement utilisés).

Le logiciel sera directement livré avec des profils disponibles

###Description fonctionnelle du projet

Le logiciel permettra à un client :
•De se créer un compte sur l'application (il pourra se connecter / deconnecter).
•De charger une image depuis son ordinateur comme "avatar" de son profil.
•De consulter un ou plusieurs article(s) présents sur le site.
•De l'ajouter à son panier.
•D'ajouter ou de retirer des articles de son panier.
•D'effectuer l'achat des articles précédents.
•De consulter ses anciens achats.

###Description technique du projet
•Java 11
•Architecture 3-tiers
•BD MySql 5
•Serveur Tomcat 9.0.X

###Outils de développement utilisés
•Frameworks utilisé : Spring MVC, Spring Boot, Sprind Data JPA
•ORM utilisé : Hibernate

Pour obtenir plus d'informations techniques, il sera nécessaire de consulter le DAT.
