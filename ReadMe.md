# Gestion PSW

##Projet de stage

###Introduction

Ce document portera sur :

-La liste des fonctionnalités indiquées dans le cahier des charges.

-La façon dont le logiciel a été développé (sa description technique ainsi que les outils de développement utilisés).

Le logiciel sera directement livré avec des profils disponibles

###Description fonctionnelle du projet

Le logiciel permettra à un client :

•De se créer un compte sur l'application (il pourra se connecter / deconnecter).

•De consulter un ou plusieurs sites.

•De consulter tous les comptes associés.

•D'ajouter ou de supprimer des comptes.

•De se créer un mot de passe robuste.

•De tester la robustesse d'un mot de passe.

###Description technique du projet

•Java 11

•Architecture 3-tiers

•BD MySql 5

•Serveur Tomcat 9.0.X

###Outils de développement utilisés

•Frameworks utilisé : Spring MVC, Spring Boot, Sprind Data JPA

•ORM utilisé : Hibernate

Pour obtenir plus d'informations techniques, il sera nécessaire de consulter le DAT.
